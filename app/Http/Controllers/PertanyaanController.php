<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;
use App\Tag;
use App\User;
use App\Pertanyaan;
use App\Profile;

class PertanyaanController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth')->only(['create', 'edit', 'update', 'delete', 'store']);
    }

    public function create(){
    	return view('pertanyaan.create');
    }

    public function store(Request $request){
    	//dd($request->all());
    	$request->validate([
    		'judul' => 'required|unique:pertanyaan|max:255',
    		'isi' => 'required'
    		]);

    	$query = DB::table('pertanyaan')->insert([
    			"judul" => $request["judul"],
    			"isi" => $request["isi"],
                "user_id" => Auth::id()
    			
    		]);

        $tags_arr = explode(',', $request['tags']);

        $tag_ids = [];
        foreach ($tags_arr as $tag_name) {
            $tag = Tag::where("tag_name", $tag_name)->first();
            if($tag){
                $tag_ids[] = $tag->id();
            } else {
                $new_tag = Tag::create(["tag_name" => $tag_name]);
                $tag_ids[] = $new_tag->id;
            }
        }

        //dd($tag_ids);

        $pertanyaan = Post::create([
            "judul" => $request["judul"],
            "isi" => $request["isi"]
            ]);

        $pertanyaan->tags()->sync($tag_ids);

        $user = Auth::user();
        $user->pertanyaan()->save($pertanyaan);

        $user->save();

    		return redirect('/pertanyaan')->with('success', 'Pertanyaan berhasil disimpan!!');
    	}

    public function index(){
        $user = Auth::user();   
        //dd($user);
        $pertanyaan = $user->pertanyaan;
        //dd($pertanyaan);
        //Auth::user()->profile->nama_lengkap
    	//$pertanyaan = DB::table('pertanyaan')->get();
    	return view('pertanyaan.index', compact('pertanyaan'));
    }

    public function show($id){
    	$pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();
    	return view('pertanyaan.show', compact('pertanyaan'));
    }

    public function edit($id){
    	$pertanyaan = DB::table('pertanyaan')->where('id', $id)->first();
    	return view('pertanyaan.edit', compact('pertanyaan'));
    }

    public function update($id, Request $request){
    	$request->validate([
    		'judul' => 'required|unique:pertanyaan',
    		'isi' => 'required'
    		]);

    	$query = DB::table('pertanyaan')
    			->where('id', $id)
    			->update ([
    			"judul" => $request["judul"],
    			"isi" => $request["isi"]	
    			]);
    			
    		return redirect('/pertanyaan')->with('success', 'Pertanyaan berhasil diupdate!!');

    }

    public function destroy($id){
    	$query = DB::table('pertanyaan')->where('id', $id)->delete();
    	return redirect('/pertanyaan')->with('success', 'Pertanyaan berhasil dihapus!');
    }
}
