<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pertanyaan extends Model
{
    //
    protected $table="pertanyaan";
    protected $fillable =["judul", "isi", "user_id"];
    
    /*protected $guarded = [];
    public $timestamps = true;*/

    public function tags(){
    	return $this->belongsToMany('App\Tag', 'pertanyaan_tag');
    }
}
